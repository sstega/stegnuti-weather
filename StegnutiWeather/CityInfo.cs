﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegnutiWeather
{
    public class Coord
    {
        public double lon, lat;
    }
    public class CityInfo
    {
        public int id;
        public string name;
        public Coord coord;

        public override bool Equals(object obj)
        {
            return id == ((CityInfo)obj).id;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }

    public class CityList
    {
        public List<CityInfo> cities;
    }
}
