﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StegnutiWeather
{
    public class Weather
    {
        public List<WeatherReading> readings;
        public WeatherReading current;

        public Weather(API.Response response)
        {
            current = new WeatherReading(response.current);
            // Construct readings
            readings = new List<WeatherReading>();

            foreach (API.HourWeather hourWeather in response.hourly)
            {
                readings.Add(new WeatherReading(hourWeather));
            }

            for (int i = 2; i < response.daily.Count; i++)
            {
                readings.Add(new WeatherReading(response.daily[i]));
            }
        }

        public List<WeatherReading> GetWeatherReadings(DateTime fromDate, DateTime toDate)
        {
            List<WeatherReading> ret = new List<WeatherReading>();

            // Generate readings up to toDate
            foreach (WeatherReading wr in readings)
            {
                if (wr.Time < fromDate || wr.Time > toDate)
                {
                    // Not within interval, skip
                    continue;
                }

                // Within interval
                ret.Add(wr);
            }

            return ret;
        }
    }
}
